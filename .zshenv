export VISUAL="MAGICK_OCL_DEVICE=OFF emacsclient -c"
export EDITOR="emacsclient -t"
export TERMINAL="tilix"

export PATH="$HOME/bin:$HOME/.local/bin:$HOME/.yarn/bin:$PATH"

#C++
export CC=clang
export CXX=clang++

#Qt
export QT_QPA_PLATFORMTHEME=qt5ct

#Java
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
export _JAVA_AWT_WM_NONREPARENTING=1

#International input
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

#FZF
export FZF_DEFAULT_OPTS="--cycle --preview 'if file -b {} | grep --quiet text; then pygmentize -g {}; elif file -b {} | grep --quiet directory; then ls -lah --color=always {}; else echo \`file -b {}\`; fi'"

#Damn it Microsoft
export DOTNET_CLI_TELEMETRY_OPTOUT=1
