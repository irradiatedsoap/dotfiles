augroup vimrcEx
    autocmd FileType c,cpp,cmake call CProject()
augroup END

function! CProject()
    if (filereadable('CMakeLists.txt'))
        let b:dispatch = 'mkdir -p ./build && cd ./build && cmake -G "Ninja" -DCMAKE_EXPORT_COMPILE_COMMANDS=TRUE -DCMAKE_BUILD_TYPE=DEBUG -DCMAKE_INSTALL_PREFIX=install -DCMAKE_CXX_FLAGS="-fcolor-diagnostics" .. && ninja && ninja install'
        set makeprg=cd\ ./build\ &&\ make
    endif
endfunction

