set nocompatible

"Vim-Plug
"Install if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('~/.local/share/nvim/plugged')

"Colorschemes
"Plug 'altercation/vim-colors-solarized'
"Plug 'blueshirts/darcula'
Plug 'kristijanhusak/vim-hybrid-material'

Plug 'nathanaelkane/vim-indent-guides'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'jistr/vim-nerdtree-tabs'
"Plug 'vim-airline/vim-airline-themes'
"Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
"Plug 'vim-syntastic/syntastic'
Plug 'scrooloose/nerdcommenter'
Plug 'jiangmiao/auto-pairs'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'Valloric/YouCompleteMe'
Plug 'xolox/vim-misc' "Required for xolox plugins
Plug 'xolox/vim-easytags'
Plug 'majutsushi/tagbar'
Plug 'easymotion/vim-easymotion'
Plug 'mbbill/undotree'
Plug 'tpope/vim-surround'
"Plug 'chrisbra/Colorizer'
"Plug 'iamcco/markdown-preview.vim'
Plug 'vim-scripts/DoxygenToolkit.vim'
Plug 'Shougo/denite.nvim', {'do': ':UpdateRemotePlugins'}
Plug 'junegunn/vim-peekaboo'
Plug 'terryma/vim-multiple-cursors'

"Languages
Plug 'sheerun/vim-polyglot'
Plug 'lervag/vimtex'

"Build, run and debug tools
Plug 'tpope/vim-dispatch'

"Language client stuff and more
"Plug 'autozimu/LanguageClient-neovim', {
    "\ 'branch': 'next',
    "\ 'do': 'bash install.sh',
    "\ }
"Plug 'junegunn/fzf'
"Plug 'roxma/nvim-completion-manager'

"Fun
Plug 'aurieh/discord.nvim', {'do': ':UpdateRemotePlugins'}

call plug#end()

"Clear autocmds
augroup vimrcEx
    autocmd!
augroup END

"Colorscheme
source $HOME/.config/nvim/colors.vim

"Vim config
source $HOME/.config/nvim/config/vim.vim

"Plugin configs
source $HOME/.config/nvim/config/vim-airline.vim
"source $HOME/.config/nvim/config/syntastic.vim
source $HOME/.config/nvim/config/YouCompleteMe.vim
source $HOME/.config/nvim/config/vim-indent-guides.vim
source $HOME/.config/nvim/config/vim-easytags.vim
source $HOME/.config/nvim/config/markdown-preview.vim
source $HOME/.config/nvim/config/vim-dispatch.vim
source $HOME/.config/nvim/config/ctrlp.vim
source $HOME/.config/nvim/config/vim-polyglot.vim
"source $HOME/.config/nvim/config/LanguageClient-neovim.vim

"Keymap
source $HOME/.config/nvim/keymap.vim

