local statics = {}

-- This is used later as the default terminal and editor to run.
statics.terminal = "tilix"
statics.editor = os.getenv("EDITOR") or "sh -c 'MAGICK_OCL_DEVICE=OFF emacs'"
statics.editor_cmd = statics.terminal .. " -e " .. statics.editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
statics.modkey = "Mod4"

-- Refresh rate
statics.refresh_rate = 60
statics.refresh_delta = 1 / statics.refresh_rate

return statics
