local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local easing = require("easing")

local mathutils = require("mathutils")
local anim = require("anim")

local avolumearc = {}

local ICON_PATH = gears.filesystem.get_xdg_config_home() ..
  "/awesome/images/material-design-icons/av/2x_web/ic_volume_up_white_48dp.png"

local icon = {
  id = "icon",
  image = ICON_PATH,
  resize = true,
  widget = wibox.widget.imagebox,
}

local icon_32 = {
  id = "icon",
  image = ICON_PATH,
  resize = true,
  forced_width = 32,
  forced_height = 32,
  widget = wibox.widget.imagebox,
}

local is_muted = false
local volume = 0

avolumearc.volume_arc = wibox.widget{
  icon,
  max_value = 1,
  thickness = 2,
  start_angle = 4.71238898, -- 2pi * 3/4
  bg = "#ffffff11",
  paddings = 2,
  widget = wibox.container.arcchart,
}

awful.tooltip {
  objects = {avolumearc.volume_arc},
  timeout = 0.1,
  timer_function = function()
    return "<b>VOLUME: </b>" ..
      string.format("%d%%", mathutils.intround(volume * 100)) .. (is_muted and " [MUTED]\n" or "\n") ..

      "Scroll wheel to change volume\n" ..
      "LClick to toggle mute\n" ..
      "RClick to show overlay"
  end,
}

avolumearc.volume_bar_popup = wibox.widget{
  max_value = 1,
  forced_width = 512,
  forced_height = 8,
  background_color = "#ffffff11",
  color = beautiful.fg_normal,
  paddings = 2,
  widget = wibox.widget.progressbar,
}

local volume_text_popup = wibox.widget{
  color = beautiful.fg_normal,
  font = "sans 18",
  widget = wibox.widget.textbox,
}

local popup = awful.popup{
  widget = {
    {
      icon_32,
      {
        avolumearc.volume_bar_popup,
        layout = wibox.container.margin(avolumearc.volume_bar_popup, 0, 0, 11, 11),
      },
      volume_text_popup,
      layout = wibox.layout.fixed.horizontal,
      spacing = 16,
    },
    margins = 10,
    widget  = wibox.container.margin
  },
  ontop = true,
  y = 100,
  visible = false,
}

local function update_bar_immediate()
  avolumearc.volume_arc.value = volume
  avolumearc.volume_bar_popup.value = volume
  avolumearc.volume_arc.colors = is_muted and {beautiful.bg_urgent} or {beautiful.bg_focus}
  avolumearc.volume_bar_popup.color = is_muted and beautiful.bg_urgent or beautiful.bg_focus
  volume_text_popup.text = string.format("%03d%%", mathutils.intround(volume * 100))
end

--- Fetch system volume levels and apply it to this widget
local function update_bar_from_system()
  awful.spawn.easy_async("amixer -D pulse sget Master", function(stdout, _, _, _)
    local mute = string.match(stdout, "%[(o%D%D?)%]")
    is_muted = mute == "off"
    local volume_match = string.match(stdout, "(%d?%d?%d)%%")
    volume = tonumber(string.format("% 3d", volume_match)) / 100

    update_bar_immediate()
  end)
end

-- Update bar every second in-case the volume changed outside of this widget
local system_update_timer = gears.timer{
  timeout = 1,
  call_now = true,
  autostart = true,
  callback = update_bar_from_system,
}

function avolumearc.hide_popup()
  popup.visible = false
  system_update_timer:again()
end

local hide_popup_timer = gears.timer{
  timeout = 3,
  autostart = false,
  callback = avolumearc.hide_popup,
  single_shot = true,
}

local function restart_popup_hide_timer()
  hide_popup_timer:again()
end

function avolumearc.show_popup()
  if not popup.visible then
    anim.animate{
      start_val = -64,
      end_val = 32,
      prop_table = popup,
      prop_name = "y",
      duration = 0.5,
      easing = easing.outExpo,
    }
    popup.x = popup.screen.geometry.width / 2 - popup.width / 2 -- Centered on the screen
    system_update_timer:stop()
  end

  popup.visible = true
  restart_popup_hide_timer()
end

function avolumearc.update_bar_and_show_popup()
  update_bar_immediate()
  avolumearc.show_popup()
end

--- Add/subtract from the current volume and show a popup
function avolumearc.user_add_volume(val)
  volume = mathutils.clamp(volume + val, 0, 1)
  awful.spawn(string.format("amixer -D pulse sset Master %f%%", math.abs(volume) * 100), false)
  avolumearc.update_bar_and_show_popup()
end

--- Toggle mute and show a popup
function avolumearc.user_toggle_mute()
  awful.spawn("amixer -D pulse sset Master toggle", false)
  is_muted = not is_muted
  avolumearc.update_bar_and_show_popup()
end

--- On click
avolumearc.volume_arc:connect_signal("button::press", function(_, _, _, button)
  if button == 4 then -- Scroll up
    avolumearc.user_add_volume(0.05)
  elseif button == 5 then -- Scroll down
    avolumearc.user_add_volume(-0.05)
  elseif button == 1 then -- LClick
    avolumearc.user_toggle_mute()
  elseif button == 3 then -- RClick
    avolumearc.show_popup()
  end
end)

return avolumearc
