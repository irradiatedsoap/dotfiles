;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (let ((user-spacemacs-local-directory (concat dotspacemacs-directory (file-name-as-directory "private") (file-name-as-directory "local"))))
    (setq-default
     ;; Base distribution to use. This is a layer contained in the directory
     ;; `+distribution'. For now available distributions are `spacemacs-base'
     ;; or `spacemacs'. (default 'spacemacs)
     dotspacemacs-distribution 'spacemacs

     ;; Lazy installation of layers (i.e. layers are installed only when a file
     ;; with a supported type is opened). Possible values are `all', `unused'
     ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
     ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
     ;; lazy install any layer that support lazy installation even the layers
     ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
     ;; installation feature and you have to explicitly list a layer in the
     ;; variable `dotspacemacs-configuration-layers' to install it.
     ;; (default 'unused)
     dotspacemacs-enable-lazy-installation 'unused

     ;; If non-nil then Spacemacs will ask for confirmation before installing
     ;; a layer lazily. (default t)
     dotspacemacs-ask-for-lazy-installation t

     ;; If non-nil layers with lazy install support are lazy installed.
     ;; List of additional paths where to look for configuration layers.
     ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
     dotspacemacs-configuration-layer-path '()

     ;; List of configuration layers to load.
     dotspacemacs-configuration-layers
     '(
       lsp
       html

       javascript
       (typescript :variables
                   typescript-backend 'lsp
                   )

       (python :variables
               python-backend 'lsp
               python-sort-imports-on-save t
               )

       (c-c++ :variables
              c-c++-enable-clang-support nil
              ;; c-c++-enable-clang-format-on-save t
              c-c++-default-mode-for-headers 'c++-mode
              c-c++-enable-rtags-support nil
              c-c++-adopt-subprojects t
              c-c++-backend 'nil ; lsp-ccls
              c-c++-lsp-sem-highlight-method 'font-lock
              c-c++-lsp-sem-highlight-rainbow t
              )

       (rust :variables
             rust-backend 'lsp
             )

       (cmake :variables
              cmake-enable-cmake-ide-support nil
              )

       lua
       csharp
       yaml
       gpu

       (latex :variables
              latex-enable-auto-fill t
              latex-enable-folding t
              latex-enable-magic t
              latex-build-command "LatexMk"
              )
       bibtex

       common-lisp

       (colors :variables
               colors-colorize-identifiers 'all
               colors-enable-nyan-cat-progress-bar nil
               )

       treemacs

       ;; ----------------------------------------------------------------
       ;; Example of useful layers you may want to use right away.
       ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
       ;; `M-m f e R' (Emacs style) to install them.
       ;; ----------------------------------------------------------------
       helm
       (auto-completion :variables
                        auto-completion-return-key-behavior 'cycle
                        auto-completion-tab-key-behavior 'complete
                        auto-completion-idle-delay 0
                        auto-completion-enable-snippets-in-popup t
                        auto-completion-enable-help-tooltip t
                        )
       better-defaults
       emacs-lisp
       git
       markdown
       ;; neotree
       org
       ;; (shell :variables
       ;;        shell-default-height 30
       ;;        shell-default-position 'bottom)
       spell-checking
       syntax-checking
       version-control
       )

     ;; List of additional packages that will be installed without being
     ;; wrapped in a layer. If you need some configuration for these
     ;; packages, then consider creating a layer. You can also put the
     ;; configuration in `dotspacemacs/user-config'.
     ;; To use a local version of a package, use the `:location' property:
     ;; '(your-package :location "~/path/to/your-package/")
     ;; Also include the dependencies as they will not be resolved automatically.
     dotspacemacs-additional-packages
     `(
       ;; From a repo online somewhere
       key-chord
       highlight-indent-guides
       smart-hungry-delete
       vue-html-mode
       vue-mode
       all-the-icons
       elcord

       ;; From a local install
       (eyebrowse-tab-bar :location ,user-spacemacs-local-directory)
       (geddit :location ,user-spacemacs-local-directory)
       (company-ismple-complete :location ,user-spacemacs-local-directory)
       )

     ;; A list of packages that cannot be updated.
     dotspacemacs-frozen-packages '()

     ;; A list of packages that will not be installed and loaded.
     dotspacemacs-excluded-packages
     '(
       indent-guides ; I use highlight-indent-guides over the one that comes with spacemacs
       )

     ;; Defines the behaviour of Spacemacs when installing packages.
     ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
     ;; `used-only' installs only explicitly used packages and deletes any unused
     ;; packages as well as their unused dependencies. `used-but-keep-unused'
     ;; installs only the used packages but won't delete unused ones. `all'
     ;; installs *all* packages supported by Spacemacs and never uninstalls them.
     ;; (default is `used-only')
     dotspacemacs-install-packages 'used-only)))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; File path pointing to emacs 27.1 executable compiled with support
   ;; for the portable dumper (this is currently the branch pdumper).
   ;; (default "emacs-27.0.50")
   dotspacemacs-emacs-pdumper-executable-file "emacs-27.0.50"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default nil)
   dotspacemacs-verify-spacelpa-archives nil

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim

   ;; If non-nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark
                         spacemacs-light)

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator slant :separator-scale 1.25)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   ;; dotspacemacs-default-font '("Source Code Pro"
   ;;                            :size 13
   ;;                            :weight normal
   ;;                            :width normal
   ;;                            :powerline-scale 1.1)

   dotspacemacs-default-font '(
                               ("scientifica"
                                :size 8
                                :weight normal
                                :width normal
                                :powerline-scale 1)

                               ("xos4 Terminus"
                                :size 8
                                :weight bold
                                :width normal
                                :powerline-scale 1)

                               ("Ubuntu Mono"
                                :size 12
                                :weight normal
                                :width normal
                                :powerline-scale 1)

                               ("Source Code Pro"
                                :size 12
                                :weight normal
                                :width normal
                                :powerline-scale 1)
                               )

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.2

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 98

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 98

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols nil

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers '(:relative t)

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'all

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."

  ;; Symlink stuff into ~/.emacs.d/private seeing as I can't put stuff in there under version control
  (defun symlink-private (&rest path)
    (let* ((target-file (string-trim-right (mapconcat 'file-name-as-directory (append (list dotspacemacs-directory "private") path) "") "/"))
           (link-file (string-trim-right (mapconcat 'file-name-as-directory (append (list (expand-file-name user-emacs-directory) "private") path) "") "/"))
           (link-dir (file-name-directory link-file)))
      (message "Symlinking private/%s if it doesn't exist" (string-trim-right (mapconcat 'file-name-as-directory path "") "/"))
      (make-symbolic-link (file-relative-name target-file link-dir) link-file t)))

  (symlink-private "local" "eyebrowse-tab-bar.el")
  (symlink-private "local" "geddit.el")
  (symlink-private "local" "company-simple-complete.el")
  (symlink-private "snippets" "latex-mode")
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."

  ;; Init packages
  (require 'eyebrowse-tab-bar)
  (require 'geddit)
  (require 'company-simple-complete)
  (require 'key-chord)
  (key-chord-mode 1)

  ;; Remap H/L to beginning/end of line
  (define-key evil-normal-state-map (kbd "H") (kbd "^"))
  (define-key evil-visual-state-map (kbd "H") (kbd "^"))
  (define-key evil-normal-state-map (kbd "L") (kbd "$"))
  (define-key evil-visual-state-map (kbd "L") (kbd "$"))

  ;; Hold C-j/k to move faster
  (define-key evil-normal-state-map (kbd "C-j") (kbd "4 j"))
  (define-key evil-visual-state-map (kbd "C-j") (kbd "4 j"))
  (define-key evil-normal-state-map (kbd "C-k") (kbd "4 k"))
  (define-key evil-visual-state-map (kbd "C-k") (kbd "4 k"))

  ;; ,, to insert ->
  (key-chord-define evil-insert-state-map ",," (kbd "->"))
  ;; ,. to insert =>
  (key-chord-define evil-insert-state-map ",." (kbd "=>"))

  ;; Sane c/c++ style please
  (c-add-style "craftedcart"
               '("java"
                 (c-basic-offset . 4)
                 (c-offsets-alist
                  (access-label . -)
                  (inclass . ++)
                  (brace-list-entry . +)
                  (case-label . +)
                  (arglist-intro . ++)
                  (arglist-close . ++)
                  (topmost-intro-cont . 0)
                  (substatement-open . 0))))`

  (defun c-align-inclass (langelem)
    (let ((inclass (assoc 'inclass c-syntactic-context)))
      (save-excursion
        (goto-char (c-langelem-pos inclass))
        (if (or (looking-at "struct") (looking-at "typedef struct"))
            '+
          '++))))
  (setq-default c-default-style "craftedcart")

  ;; Other indentation levels
  (setq cmake-tab-width 4)

  ;; Highlight indent guides
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-responsive 'stack)
  (setq highlight-indent-guides-delay 0.1)

  ;; Don't use the system clipboard by default, require prefixing yank/delete/etc. with "+
  (setq x-select-enable-clipboard nil)

  ;; Hungry deletion
  (require 'smart-hungry-delete)
  (smart-hungry-delete-add-default-hooks)
  (define-key evil-insert-state-map (kbd "DEL") 'smart-hungry-delete-backward-char)
  (define-key evil-insert-state-map (kbd "<deletechar>") 'smart-hungry-delete-forward-char)
  (define-key evil-insert-state-map (kbd "<delete>") 'smart-hungry-delete-forward-char)

  ;; C-l to redraw, without recentering
  (define-key evil-normal-state-map (kbd "C-l") 'redraw-display)

  ;; Layouts
  (define-key evil-normal-state-map (kbd "M-=") 'eyebrowse-create-window-config)
  (define-key evil-normal-state-map (kbd "M--") 'eyebrowse-close-window-config)
  (define-key evil-normal-state-map (kbd "M-]") 'eyebrowse-next-window-config)
  (define-key evil-normal-state-map (kbd "M-[") 'eyebrowse-prev-window-config)

  ;; Show a file browser when creating a new workspace (If we're in a project)
  (defun on-new-workspace-find-file ()
    (when (projectile-project-p)
      (message "Opening file browser...")
      (helm-projectile-find-file)))
  (defun on-new-workspace ()
    (run-at-time 0.1 nil 'on-new-workspace-find-file))
  (setq eyebrowse-new-workspace 'on-new-workspace)

  ;; Web dev indentation
  (setq-default coffee-tab-width 2) ; coffeescript
  (setq-default javascript-indent-level 2) ; javascript-mode
  (setq-default js-indent-level 2) ; js-mode
  (setq-default js2-basic-offset 2) ; js2-mode, in latest js2-mode, it's alias of js-indent-level
  (setq-default web-mode-markup-indent-offset 2) ; web-mode, html tag in html file
  (setq-default web-mode-css-indent-offset 2) ; web-mode, css in html file
  (setq-default web-mode-code-indent-offset 2) ; web-mode, js code in html file
  (setq-default css-indent-offset 2) ; css-mode

  ;; Auto update LaTeX previews
  (add-hook 'doc-view-mode-hook 'auto-revert-mode)

  ;; 4 width tabs
  (setq-default tab-width 4)

  ;; 120 column fill
  (setq-default fill-column 120)
  (setq flycheck-flake8-maximum-line-length 120)

  ;; Make prefixing lisp names easier
  (defun insert-filename-base ()
    (interactive)
    (insert (file-name-base (buffer-file-name)))
    (insert "-"))
  (key-chord-define evil-insert-state-map "_+" 'insert-filename-base)

  ;; Smaller treemacs icons
  (treemacs-resize-icons 12)

  (with-eval-after-load 'lua-mode
    ;; Override this func as I never want some brace to be indented in aliment with the starting brace - always further back
    (defun lua-calculate-indentation-override (&optional parse-start)
      "Return overriding indentation amount for special cases.

If there's a sequence of block-close tokens starting at the
beginning of the line, calculate indentation according to the
line containing block-open token for the last block-close token
in the sequence.

If not, return nil."
      (let (case-fold-search token-info block-token-pos)
        (save-excursion
          (if parse-start (goto-char parse-start))

          (back-to-indentation)
          (unless (lua-comment-or-string-p)
            (while
                (and (looking-at lua-indentation-modifier-regexp)
                     (setq token-info (lua-get-block-token-info (match-string 0)))
                     (not (eq 'open (lua-get-token-type token-info))))
              (setq block-token-pos (match-beginning 0))
              (goto-char (match-end 0))
              (skip-syntax-forward " " (line-end-position)))

            (when (lua-goto-matching-block-token block-token-pos 'backward)
              (current-indentation))))))

    ;; Override this func as I never want indentation to be far in if say, we're declaring a multiline function in the parens when calling a func
    ;; Eg: something(function() ...\n... end)
    (defun lua-make-indentation-info-pair (found-token found-pos)
      "This is a helper function to lua-calculate-indentation-info. Don't
use standalone."
      (cond
       ;; function is a bit tricky to indent right. They can appear in a lot ot
       ;; different contexts. Until I find a shortcut, I'll leave it with a simple
       ;; relative indentation.
       ;; The special cases are for indenting according to the location of the
       ;; function. i.e.:
       ;;       (cons 'absolute (+ (current-column) lua-indent-level))
       ;; TODO: Fix this. It causes really ugly indentations for in-line functions.
       ((string-equal found-token "function")
        (cons 'relative lua-indent-level))

       ;; block openers
       ((member found-token (list "{" "(" "["))
        (save-excursion
          (let ((found-bol (line-beginning-position)))
            (forward-comment (point-max))
            ;; If the next token is on this line and it's not a block opener,
            ;; the next line should align to that token.
            (if (and (zerop (count-lines found-bol (line-beginning-position)))
                     (not (looking-at lua-indentation-modifier-regexp)))
                (cons 'absolute (current-indentation))
              (cons 'relative lua-indent-level)))))

       ;; These are not really block starters. They should not add to indentation.
       ;; The corresponding "then" and "do" handle the indentation.
       ((member found-token (list "if" "for" "while"))
        (cons 'relative 0))
       ;; closing tokens follow: These are usually taken care of by
       ;; lua-calculate-indentation-override.
       ;; elseif is a bit of a hack. It is not handled separately, but it needs to
       ;; nullify a previous then if on the same line.
       ((member found-token (list "until" "elseif"))
        (save-excursion
          (let ((line (line-number-at-pos)))
            (if (and (lua-goto-matching-block-token found-pos 'backward)
                     (= line (line-number-at-pos)))
                (cons 'remove-matching 0)
              (cons 'relative 0)))))

       ;; else is a special case; if its matching block token is on the same line,
       ;; instead of removing the matching token, it has to replace it, so that
       ;; either the next line will be indented correctly, or the end on the same
       ;; line will remove the effect of the else.
       ((string-equal found-token "else")
        (save-excursion
          (let ((line (line-number-at-pos)))
            (if (and (lua-goto-matching-block-token found-pos 'backward)
                     (= line (line-number-at-pos)))
                (cons 'replace-matching (cons 'relative lua-indent-level))
              (cons 'relative lua-indent-level)))))

       ;; Block closers. If they are on the same line as their openers, they simply
       ;; eat up the matching indentation modifier. Otherwise, they pull
       ;; indentation back to the matching block opener.
       ((member found-token (list ")" "}" "]" "end"))
        (save-excursion
          (let ((line (line-number-at-pos)))
            (lua-goto-matching-block-token found-pos 'backward)
            (if (/= line (line-number-at-pos))
                (lua-calculate-indentation-info (point))
              (cons 'remove-matching 0)))))

       ;; Everything else. This is from the original code: If opening a block
       ;; (match-data 1 exists), then push indentation one level up, if it is
       ;; closing a block, pull it one level down.
       ('other-indentation-modifier
        (cons 'relative (if (nth 2 (match-data))
                            ;; beginning of a block matched
                            lua-indent-level
                          ;; end of a block matched
                          (- lua-indent-level)))))))

  (setq spacemacs-user-config-loaded t)
  )

(defun gui-init ()
  ;; We don't get powerline symbols if we use emacsclient after doing the systemctl thing unless we do this
  (setq powerline-default-separator 'slant)

  ;; Transparency
  ;; (spacemacs/toggle-transparency)
  (set-frame-parameter (selected-frame) 'alpha
                       (cons dotspacemacs-active-transparency
                             dotspacemacs-inactive-transparency))

  ;; All the icons!
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))

  ;; Fix powerline-height
  (setq powerline-scale 1
        powerline-height (spacemacs/compute-mode-line-height)
        )

  ;; Fix highlight-indent-guides
  (when (bound-and-true-p spacemacs-user-config-loaded)
    (highlight-indent-guides-auto-set-faces)
    )

  ;; Fix bug https://github.com/syl20bnr/spacemacs/issues/10638 where Python autocomplete doesn't work
  ;; (eval-after-load "company"
  ;;   '(add-to-list 'company-backends 'company-anaconda))
  )

(spacemacs|do-after-display-system-init
 (gui-init)
 )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.

(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
  (custom-set-variables
   ;; custom-set-variables was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(evil-want-Y-yank-to-eol nil)
   '(org-agenda-files (quote ("~/Documents/kurami/agenda.org" "~/org/notes.org")))
   '(package-selected-packages
     (quote
      (company-lua lua-mode tide typescript-mode slime-company slime common-lisp-snippets frame-purpose esxml a tracking ov anaphora treemacs-projectile treemacs-evil treemacs pfuture clojure-snippets cider-eval-sexp-fu cider sesman queue clojure-mode selectric-mode elcord rainbow-mode rainbow-identifiers color-identifiers-mode yasnippet-snippets web-mode org-projectile org-download json-navigator google-translate evil-nerd-commenter evil-magit dumb-jump doom-modeline diff-hl define-word counsel-projectile counsel swiper company-lsp cmake-ide centered-cursor-mode ace-window ace-link auctex smartparens company projectile window-purpose avy lsp-mode flycheck pdf-tools tablist ivy helm helm-core magit powerline dash visual-fill-column org-plus-contrib hydra yapfify yaml-mode ws-butler writeroom-mode winum which-key web-beautify vue-mode volatile-highlights vi-tilde-fringe uuidgen use-package unfill toml-mode toc-org tagedit symon string-inflection spaceline-all-the-icons smeargle smart-hungry-delete slim-mode shrink-path scss-mode sass-mode restart-emacs rainbow-delimiters racer pyvenv pytest pyenv-mode py-isort pug-mode prettier-js popwin pippel pipenv pip-requirements persp-mode pcre2el password-generator paradox overseer orgit org-ref org-present org-pomodoro org-mime org-category-capture org-bullets org-brain opencl-mode open-junk-file omnisharp neotree nameless mwim move-text markdown-toc magit-svn magit-gitflow magic-latex-buffer macrostep lsp-ui lorem-ipsum livid-mode live-py-mode link-hint levenshtein json-mode js2-refactor js-doc indent-guide importmagic impatient-mode imenu-list hungry-delete ht hl-todo highlight-parentheses highlight-numbers highlight-indentation highlight-indent-guides hierarchy helm-xref helm-themes helm-swoop helm-rtags helm-pydoc helm-purpose helm-projectile helm-org-rifle helm-mode-manager helm-make helm-gitignore helm-git-grep helm-flx helm-descbinds helm-ctest helm-css-scss helm-company helm-c-yasnippet helm-ag google-c-style golden-ratio gnuplot glsl-mode gitignore-templates gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe git-gutter-fringe+ gh-md fuzzy font-lock+ flyspell-correct-helm flycheck-rust flycheck-rtags flycheck-pos-tip flx-ido fill-column-indicator fancy-battery eyebrowse expand-region evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-org evil-numbers evil-matchit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state evil-goggles evil-exchange evil-escape evil-ediff evil-cleverparens evil-args evil-anzu eval-sexp-fu emmet-mode elisp-slime-nav eldoc-eval editorconfig dotenv-mode disaster diminish cython-mode cuda-mode cquery company-web company-tern company-statistics company-rtags company-quickhelp company-glsl company-c-headers company-auctex company-anaconda column-enforce-mode cmake-mode clean-aindent-mode clang-format ccls cargo browse-at-remote auto-yasnippet auto-highlight-symbol auto-dictionary auto-compile auctex-latexmk aggressive-indent ace-jump-helm-line ac-ispell))))
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   )
  )
