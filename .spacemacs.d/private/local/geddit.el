;;; geddit.el --- Automagically create setters and getters for C++ -*- lexical-binding: t -*-

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;;; Code:

(defun geddit-insert-getter-and-setter ()
  "Insert public getter and setter functions for the variable at
the current line"
  (interactive)
  (let* ((line (string-trim-left (thing-at-point 'line t)))
         (var-split (geddit--split-at-first-char line ?\s))
         (var-type (car var-split))
         (var-name (car (geddit--split-at-any-first-char (cdr var-split) '(?\s ?=)))))
    (geddit--insert-c++-header-getter-and-setter var-type var-name)))

(defun geddit--insert-c++-header-getter-and-setter (var-type var-name)
  "Find the next public: section in a C++ class (or create one at
the end if it doesn't exist) and insert getter and setter
functions at the end of the access modifier section"
  (let ((insert-newline-before t))
    (condition-case nil
        (search-forward "public:")
      (search-failed
       (geddit--goto-end-of-c++-class)
       (end-of-line 0)
       (insert "\n\npublic:")
       (indent-according-to-mode)
       (setq insert-newline-before nil)))

    (geddit--goto-end-of-c++-access-modifier)
    (end-of-line 0)
    (when insert-newline-before (newline))
    (newline-and-indent)
    (insert (format "%s get%s();" var-type (upcase-initials var-name)))
    (newline-and-indent)
    (insert (format "void set%s(%s %s);" (upcase-initials var-name) var-type var-name))))

(defun geddit--goto-end-of-c++-class ()
  "Jump to the closing semicolon of a C++ class"
  (word-search-backward "class")
  (search-forward "{")
  (backward-char)
  (forward-list))

(defun geddit--goto-end-of-c++-access-modifier ()
  "Move point to the end of the current C++ access modifier in a
class, or to the end of a class if this is the last access
modifier"
  (condition-case nil
      (progn
        (re-search-forward "public:\\|protected:\\|private:")
        (end-of-line 0))
    (search-failed (geddit--goto-end-of-c++-class))))

(defun geddit--split-at-first-char (str char)
  "Return a cons cell where car is a substring of STR up to but
not including the first instance of CHAR, and cdr is the rest of
the string not including the first instance of CHAR.

If STR does not contain CHAR, return nil."
  (catch 'found
    (dotimes (i (length str))
      (when (eq (aref str i) char)
        (throw 'found (cons (substring str 0 i) (substring str (1+ i))))))))

(defun geddit--split-at-any-first-char (str char)
  "Return a cons cell where car is a substring of STR up to but
not including the first instance of any character in the list
CHAR, and cdr is the rest of the string not including the first
instance of the character found.

If STR does not contain any character in CHAR, return nil."
  (catch 'found
    (dotimes (i (length str))
      (dolist (c char)
        (when (eq (aref str i) c)
          (throw 'found (cons (substring str 0 i) (substring str (1+ i)))))))))

(provide 'geddit)

;;; geddit.el ends here
